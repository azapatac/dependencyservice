﻿namespace DependencyService.Interfaces
{
    using Xamarin.Forms.Internals;

    public interface IDeviceOrientationService
    {
        DeviceOrientation GetOrientation();
    }
}