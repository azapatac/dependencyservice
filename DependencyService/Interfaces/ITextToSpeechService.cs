﻿namespace DependencyService.Interfaces
{
    using System.Threading.Tasks;

    public interface ITextToSpeechService
    {
        Task SpeakAsync(string text);
    }
}