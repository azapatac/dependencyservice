﻿namespace DependencyService
{
    using System;
    using DependencyService.Interfaces;
    using Xamarin.Forms;

    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            var service = DependencyService.Get<IDeviceOrientationService>();

            var orientation = service.GetOrientation();

            App.Current.MainPage.DisplayAlert("Orientation", orientation.ToString(), "Ok");
        }

        async void Button_Clicked_1(System.Object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(myEntry.Text))
            {
                return;
            }

            ITextToSpeechService service = DependencyService.Get<ITextToSpeechService>(DependencyFetchTarget.NewInstance);
            using (service as IDisposable)
            {
                await service.SpeakAsync(myEntry.Text);
            }
        }
    }
}